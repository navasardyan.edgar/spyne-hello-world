# spyne-hello-world


Hello world app consisting of a SOAP server (using `Spyne`) and client (using `Suds` lib) apps.

## Calling remote procedure from Postman

Here is the example Body

```
<SOAP-ENV:Envelope 
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:ns0="spyne.examples.hello.soap" 
    xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/">
  <SOAP-ENV:Header />
  <ns1:Body>
    <ns0:say_hello>
      <ns0:name>punk</ns0:name>
      <ns0:times>5</ns0:times>
    </ns0:say_hello>
  </ns1:Body>
</SOAP-ENV:Envelope>
```


## Suds

Suds is a lightweight  allowing to call web service methods without the need to generate XML files (this is my unproved interpetation of what Suds is), just like that: 
`c.service.say_hello('punk', 5)`


Actually, it transforms the input params into an xml schema behind the scenes. To see the actual file, try this:
`print(etree.tostring(ctx.in_document))`

## Useful reads

Docs - http://spyne.io/docs/2.10/manual/02_helloworld.html
Suds - https://webkul.com/blog/python-suds-client/#:~:text=Suds%20is%20a%20lightweight%20library,service%20proxy%20for%20web%20services.

## To-do

Read the source code of Spyne and Suds
